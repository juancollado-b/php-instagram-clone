<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store()
    {
        $data = request()->validate([
            'content'=>'required',
            'post_id'=>'required'
        ]);

        $authUser = auth()->user();
        $newComment = $authUser->comments()->create($data);
        $newComment->load('user');

        return $newComment;
    }
}
