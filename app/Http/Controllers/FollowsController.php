<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class FollowsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function store(User $user)
    {
        // auth()->user() is the authenticated user
        // And $user is the profile's user
        return auth()->user()->following()->toggle($user->profile);
    }
}
