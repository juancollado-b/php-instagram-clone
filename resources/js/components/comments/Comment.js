import React from 'react'

const Comment = (props) => (
    <div className="d-flex justify-content-between align-items-center mb-4">
        <div>
            <a href={"/profile/" + props.userId} className="text-decoration-none">
                <span className="fw-bold text-dark pe-2">{props.username}</span>
            </a>
            {props.content}
        </div>
        <div>
            <span className="ps-3 text-muted comment-date">{props.date}</span>
        </div>
    </div>
)


export default Comment