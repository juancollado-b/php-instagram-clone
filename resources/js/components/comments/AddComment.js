import React from 'react'

export default class AddComment extends React.Component {
    handleFormSubmit = (e) => {
        e.preventDefault()
        
        const content = e.target.elements.content.value
        this.props.handleAddComment(content)
    }

    render = () => (
        <div>
            <form onSubmit={this.handleFormSubmit}>
            <div className="input-group mt-5">
                <input type="text" className="form-control" name="content"/>
                <button className="btn btn-primary" type="submit" id="button-addon1">Send</button>
            </div>
            </form>
        </div>
    )
}