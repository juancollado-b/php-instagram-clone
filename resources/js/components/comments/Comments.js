import React from 'react'
import Comment from './Comment'
import AddComment from './AddComment'


const Comments = (props) => (
    <div>
    {
        props.comments.map((comment) => (
            <Comment
            key={comment.username+comment.content} 
            username={comment.username}
            content={comment.content}
            date={comment.date}
            userId={comment.userId}
        />
        ))
    }
    { props.isAuth && <AddComment handleAddComment={props.handleAddComment}/>}
    </div>
)

export default Comments