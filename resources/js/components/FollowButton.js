import React from 'react'

export default class FollowButton extends React.Component {
    state = {
        isFollowing: this.props.isFollowing
    }

    handleFollowAction = async () => {
        console.log(this.props.user)
        try {
            const response = await axios.post('/follow/' + this.props.user)
            this.setState((prevState) => ({ isFollowing: !prevState.isFollowing }))
        } catch (error) {
            if (error.response.status == 401) {
                window.location = "/login"
            }
        }
    }

    render = () => (
        <div>
            <button className="btn btn-primary" onClick={this.handleFollowAction}>{this.state.isFollowing ? 'Unfollow' : 'Follow'}</button>
        </div>
    )
}
