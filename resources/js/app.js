require('./bootstrap');
import ReactDOM from 'react-dom'

import FollowButton from './components/FollowButton'
import './components/comments/CommentsSection'

const $followButton = document.getElementById('follow-button')

if ($followButton) {
    ReactDOM.render(<FollowButton
        user={$followButton.dataset.user}
        isFollowing={$followButton.dataset.follows ? true : false}
        />, document.getElementById('follow-button'))
}