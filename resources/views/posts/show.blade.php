@extends('layouts.app')

@section('content')
<div class="container">

<div class="row">
    <div class="col-8">
        <img src="/storage/{{$post->image}}" class="w-100">
    </div>
    <div class="col-4">
        <div>
            <div class="d-flex align-items-center justify-content-between">
                <div>
                    <img src="/storage/{{ $post->user->profile->image }}" class="rounded-circle w-100 post-profile">
                    <a href="/profile/{{$post->user->id}}" class="text-decoration-none text-dark ps-2 fw-bold">
                        {{ $post->user->username }}
                    </a>
                </div>
                @can('delete', $post)
                    <div>
                        <form action="/p/{{$post->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </div>
                @endcan
            </div>

            <hr>

            <div class="d-flex justify-content-between align-items-center mb-4">
                <div>
                    <a href="/profile/{{$post->user->id}}" class="text-decoration-none">
                        <span class="fw-bold text-dark pe-2">{{ $post->user->username }}</span>
                    </a>
                    {{ $post->caption }}
                </div>
                <div>
                    <span class="ps-3 text-muted comment-date">{{ date('d/m/y',strtotime($post->created_at)) }}
                </div>
            </div>
        </div>
        <div id="comments-section" data-comments="{{$post->comments}}" data-postId="{{$post->id}}" @auth data-auth="true" @endauth></div>
    </div>
</div>

</div>
@endsection